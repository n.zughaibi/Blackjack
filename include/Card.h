//
// Created by NAZ on 7/5/2018.
//

#ifndef BLACKJACK_CARD_H
#define BLACKJACK_CARD_H


#include <string>
#include <vector>
#include <iostream>

/**
 * Header to hold the Card objects. Down the line might implement something with suits
 * but for blackjack that's useless (I think).
 */
using namespace std;
class Card {
private:
    string value;
    std::string suit;
public:
    Card(int value);

    Card();

//    Card(int value, string suit);

    int getActualValue();

    string getValue() const;

    void setValue(string value);

    const std::string &getSuit() const;

    void setSuit(const std::string &suit);

    string buildCards(vector<Card> cards);
};


#endif //BLACKJACK_CARD_H
