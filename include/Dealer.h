//
// Created by NAZ on 7/4/2018.
//

#ifndef BLACKJACK_DEALER_H
#define BLACKJACK_DEALER_H


#include "Card.h"
#include <vector>

/**
 * Class to hold Dealer details
 * TODO: on second thought this class is kind of useless. Just create another instance of player
 */
class Dealer {
public:
    Dealer();

    int getNumOfCards() const;

    void setNumOfCards(int numOfCards);


    const std::vector<Card> &getCards() const;

    void setCards(const std::vector<Card> &cards);

    bool isWinner() const;

    void setWinner(bool winner);

    int getCardTotal();

    int checkCardValue(string preNormalizedValue);

private:
    int numOfCards;
    std::vector<Card> cards;
    bool winner;

};


#endif //BLACKJACK_DEALER_H
