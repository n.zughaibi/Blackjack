//
// Created by NAZ on 7/5/2018.
//

#ifndef BLACKJACK_PLAYER_H
#define BLACKJACK_PLAYER_H


#include <vector>
#include "Card.h"
/**
 * Class to hold the players information.
 */
class Player {
public:
    Player();
private:
    int numOfCards;
    int cardTotal;
    std::vector<Card> cards;
    bool winner;

public:
    int getNumOfCards() const;

    void setNumOfCards(int numOfCards);

    const vector<Card> &getCards() const;

    void setCards(const vector<Card> &cards);

    bool isWinner() const;

    void setWinner(bool winner);

    int getCardTotal();

    int checkCardValue(string preNormalizedCard);
};


#endif //BLACKJACK_PLAYER_H
