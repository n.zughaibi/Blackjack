/**
 * Card class to keep track of the value of the cards
 */

#include "../include/Card.h"
string cards[13] = {"A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "K", "Q", "J"};

Card::Card(int value) {
    setValue(cards[value]);
}

string Card::getValue() const {
    return value;
}

void Card::setValue(string value) {
    Card::value = value;
}

const std::string &Card::getSuit() const {
    return suit;
}

void Card::setSuit(const std::string &suit) {
    Card::suit = suit;
}

Card::Card() {}

/**
 * Builds the cards to show the user
 * @param cards The cards where we mainly care about their values
 * @return
 */
string Card::buildCards(vector<Card> cards) {
    string cardPrint;

    vector<Card>::iterator it;

    for (it = cards.begin(); it < cards.end() ; ++it) {
        cardPrint += (string("-------------    "));
        cout << "-----------------     " << "" ;
    }

    cardPrint += "\n";
    cout << "\n";

    for (it = cards.begin(); it < cards.end() ; ++it) {

//        int value = it->getValue();

        //Deals with when there's single digits, double digits, and logic to not show the dealers second card in hand.
        if(it->getValue().length() > 1){

            cardPrint += (string("|              ") + it->getValue() +" |       ");
            cout << "|            " << it->getValue() << " |     " << "";

        }else if(it->getValue().length() > 1 && (it != cards.end()-1)) {
            cardPrint += (string("|              ") + it->getValue() +" |       ");
            cout << "|            " << it->getValue() << " |     " << "";
        }else{

                cardPrint += (string("|              ") + it->getValue() +" |       ");
                cout << "|            " << it->getValue() << "  |     " << "";

        }
    }



    cardPrint += "\n";
    cout << "\n";


    //Rest of the body
    for (int i = 0; i < 4; ++i) {
        for (it = cards.begin(); it < cards.end() ; ++it) {
            cardPrint += (string("|               |      "));
            cout << "|               |     " <<  "";
        }

        cardPrint += "\n";
        cout << "\n";

    }

    for (it = cards.begin(); it < cards.end() ; ++it) {
//        int value = it->getValue();

        if(it->getValue().length() > 1){
            cardPrint += (string("|    ") + it->getValue() +"           |       ");
            cout << "|    " << it->getValue() << "         |     " << "";
        }else if(it->getValue().length() > 1 && (it != cards.end()-1)) {
            cardPrint += (string("|    ") + it->getValue() +"           |       ");
            cout << "|    " << it->getValue() << "         |     " << "";
        }else{
            cardPrint += (string("|    ") + it->getValue() +"           |       ");
            cout << "|    " << it->getValue() << "          |     " << "";
        }


    }

    cardPrint =+ "\n";
    cout << "\n";


    for (it = cards.begin(); it < cards.end() ; ++it) {
        cardPrint += (string("-------------    "));
        cout << "-----------------     " << "" ;
    }

    cardPrint += "\n";
    cout << "\n";


    /*const string temp =string("\n-------------") +string("           -------------")
                       + string("\n|         ") + to_string(card1)+" |          " + string("|         ") + to_string(card2)+" |"
                       +string("\n|            |") + string("          ") + string("|           |")
                       +string("\n|            |") + string("          ") + string("|           |")
                       +string("\n|            |") + string("          ") + string("|           |")
                       + string("\n| ") +to_string(card1)+"         |" + string("          | ") +to_string(card2)+"         |"
                       + string("\n-------------") + string("           -------------");
    return temp;*/

    return std::string();
}

/**
 * Returns the face cards actual value for calculation
 * @return actualValue The cards actual value.
 */
int Card::getActualValue() {
    int actualValue;
    string preNormalizedCard = getValue();
    if (preNormalizedCard == "A" ||
        preNormalizedCard == "J" ||
        preNormalizedCard == "Q" ||
        preNormalizedCard == "K"){
        actualValue = 10;
    }else{
        actualValue = stoi(preNormalizedCard);
    }

    return actualValue;
}
