//
// Created by NAZ on 7/4/2018.
//

#include "../include/Dealer.h"


int Dealer::getNumOfCards() const {
    return numOfCards;
}


void Dealer::setNumOfCards(int numOfCards) {
    Dealer::numOfCards = numOfCards;
}


Dealer::Dealer() {

}

const std::vector<Card> &Dealer::getCards() const {
    return cards;
}

void Dealer::setCards(const std::vector<Card> &cards) {
    Dealer::cards = cards;
}

bool Dealer::isWinner() const {
    return winner;
}

void Dealer::setWinner(bool winner) {
    Dealer::winner = winner;
}

int Dealer::getCardTotal() {
    vector<Card>::iterator it;
    int total = 0;
    for (it = cards.begin(); it < cards.end() ; ++it) {
        total = total + checkCardValue(it->getValue());

    }

    return total;
}

/**
 * Normalize the card values. Used for facecards and all.
 * @param preNormalizedCard Number passed before being added to user/dealer deck
 * @return Normalized value
 */
int Dealer::checkCardValue(string preNormalizedCard){
    int actualValue;

    if (preNormalizedCard == "A" ||
        preNormalizedCard == "J" ||
        preNormalizedCard == "Q" ||
        preNormalizedCard == "K"){
        actualValue = 10;
    }else{
        actualValue = stoi(preNormalizedCard);
    }

    return actualValue;
}

