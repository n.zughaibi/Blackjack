//
// Created by NAZ on 7/5/2018.
//

#include "../include/Player.h"

int Player::getNumOfCards() const {
    return numOfCards;
}

void Player::setNumOfCards(int numOfCards) {
    Player::numOfCards = numOfCards;
}

const vector<Card> &Player::getCards() const {
    return cards;
}

void Player::setCards(const vector<Card> &cards) {
    Player::cards = cards;
}

Player::Player() {

}

bool Player::isWinner() const {
    return winner;
}

void Player::setWinner(bool winner) {
    Player::winner = winner;
}

int Player::getCardTotal() {
    vector<Card>::iterator it;
    int total = 0;
    for (it = cards.begin(); it < cards.end() ; ++it) {
        total = total + checkCardValue(it->getValue());
    }

    return total;
}

/**
 * Normalize the card values. Used for facecards and all.
 * @param preNormalizedCard Number passed before being added to user/dealer deck
 * @return Normalized value
 */
int Player::checkCardValue(string preNormalizedCard){
    int actualValue;

    if (preNormalizedCard == "A" ||
        preNormalizedCard == "J" ||
        preNormalizedCard == "Q" ||
        preNormalizedCard == "K"){
        actualValue = 10;
    }else{
        actualValue = stoi(preNormalizedCard);
    }

    return actualValue;
}