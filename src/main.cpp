#include <iostream>
#include "../include/Dealer.h"
#include "../include/Player.h"
#include <sstream>
#include <ctime>
#include "random"
#include <stdlib.h> // For random(), RAND_MAX

using namespace std;

//Function declarations
void showCards();
void endGame();
int randomNumberInRange(int min, int max);
int hitOrStay();
void gameLoop();
void calculateCards(int userCardTotals, int dealerCardTotals, bool isDealer);
void dealCards(int forUser, int forDealer);

//Instantiate variables
enum faceCards{
    King,
    Queen,
    Jack,
    Ace
};
int MAX_CARDS = 12;

Player player;
Dealer dealer;

int main() {

    cout << "Welcome to BlackJack! To exit the program enter 0" << "\n";

    cout << "Dealing your cards..." << "\n";
    dealCards(2, 2);

    showCards();


    gameLoop();

    return 0;
}

/**
 * Deal the user and dealer cards when requested and on load.
 * @param forUser How many for the user
 * @param forDealer How many for the dealer
 */
void dealCards(int forUser, int forDealer) {
    vector<Card> userCards;
    vector<Card> dealerCards;
    if (player.getCards().size() != 0 && dealer.getCards().size() != 0){
        userCards = player.getCards();
        dealerCards = dealer.getCards();
//        cout << "reaches here" << endl;
//        cout << "Player cards: " << player.getCards().size() << endl;
    }

    if (forUser != 0) {
        for (int i = 0; i < forUser; ++i) {
            int randomDeal = randomNumberInRange(0, MAX_CARDS);
            //cout << "\n random user deal: " << randomDeal << "\n";

            Card card(randomDeal);
            userCards.push_back(card);
        }
    }
    if(forDealer != 0) {
        for (int i = 0; i < forDealer; ++i) {

            int randomDeal = randomNumberInRange(0, MAX_CARDS);
            //cout << "\n random user deal: " << randomDeal << "\n";

            Card card(randomDeal);
            dealerCards.push_back(card);
        }
    }

    player.setCards(userCards);
    dealer.setCards(dealerCards);
}

/**
 * Generate a random number in the given range.
 * @param min Lower end
 * @param max Maximum end
 * @return randomized number within the range.
 */
int randomNumberInRange(int min, int max){
    // Random seed
    random_device rd;
    // Initialize Mersenne Twister pseudo-random number generator
    mt19937 gen(rd());

    // Generate pseudo-random numbers
    // uniformly distributed in range (1, 100)
    uniform_int_distribution<> dis(0, MAX_CARDS/*sizeof(cards)/ sizeof(cards[0]) */);

    //cout << "Card size: " <<  sizeof(cards)/ sizeof(cards[0])  << "\n";
    return dis(gen);
}

/**
 * Gathers input from user to either hit or stay.
 * @return The choice the user makes (0 being quit).
 */
int hitOrStay(){
    int answer;

    do{
        string input;

        cout << "Hit(1) or Stay(2)?  Enter 0 if you wish to quit." << endl;
        cin >> input;
//        cout << "input: " << input << endl;
        answer = stoi(input);

        if (answer != 1 && answer != 2 && answer != 0 ){
            cout << "Error: Invalid option" << endl;
        }

    }while (answer != 1 && answer != 2 && answer != 0 );

    calculateCards(player.getCardTotal(), dealer.getCardTotal(), false);

    return answer;

}

/**
 * Builds the deck under Card.buildCards() for user and dealer.
 * @param hideDealerCard Whether or not to show the dealers last card on initial deal.
 */
void showCards(){
    //Show cards on table:
    Card card;
    cout << "Your cards: " << "\n";
    card.buildCards(player.getCards());
    cout << "Dealer cards: " << "\n";
    card.buildCards(dealer.getCards());

}

/**
 * Main game loop hit/stay and winner decider.
 */
void gameLoop(){

    int input;


    do{

        if(dealer.getCardTotal() < 17){
            cout << "dealer hits" << "\n";
            dealCards(0,1);
            showCards();
        }

        input = hitOrStay();
        switch (input){
            case 1:
                dealCards(1,0);
                showCards();
//                calculateCards(player.getCardTotal(), dealer.getCardTotal(), false);
                break;
            case 2:
                calculateCards(player.getCardTotal(), dealer.getCardTotal(), true);
                break;

        }
    }while(input != 0 && !player.isWinner() && !dealer.isWinner());

    endGame();
}
/**
 * Calculate the card totals and declare a winner.
 * @param userCardTotals Total value of the users cards
 * @param dealerCardTotals Total value of the dealers cards.
 * @param finalHand Whether or not this is the final hand by the user.
 */
void calculateCards(int userCardTotals, int dealerCardTotals, bool finalHand){

    //showCards();
    bool userWon;

    cout << "user total: " << userCardTotals << endl;
    cout << "dealer total: " << dealerCardTotals << endl;
    userWon = userCardTotals == 21 &&
              (dealerCardTotals > 21 || dealerCardTotals < 20) ||
              (userCardTotals < 21 && dealerCardTotals > 21) ||
              (userCardTotals > dealerCardTotals && userCardTotals <= 21);

    if (!finalHand && userWon && (userCardTotals > 21 || dealerCardTotals > 21)){
        player.setWinner(true);
    }else if(finalHand && userWon){
        player.setWinner(true);
    }else if(finalHand && userCardTotals == dealerCardTotals){
        cout << "Draw! No winners here!" << endl;
    }else if(finalHand && !userWon){
        dealer.setWinner(true);
    }else if(dealerCardTotals > 21){
        player.setWinner(true);
    }else if(userCardTotals > 21){
        dealer.setWinner(true);
    }

    if (player.isWinner() || dealer.isWinner()){
        endGame();
    }
}

/**
 * Ends the game.
 */
void endGame(){
    if(player.isWinner()){
        cout << "Congrats! You win!" << endl;
    } else{
        cout << "Better luck next time! Dealer wins!" << endl;
    }

    exit(EXIT_SUCCESS);
}
